import numpy as np

'''
etscales computes whichever equal temperament scale (ETs) you want and stores it in an array. 
We can formalize any ETs by the geometric progression: f_n = f_0 * np.power(I, n/N) (1)
where f_n is the frequency at the degree n, f_0 is the generating frequency, I is the interval to divide (2 is an octave, 3/2 a fifth,etc)
and N is the number of divisions of I.
We can also define the max frequency of the scale by resolving (1) and find n. We have to convert it as an int to ensure that the for loop works.
'''

def etscales(gen_frequency, interval_to_divide, number_of_divisions, max_frequency):
    
    scl = [(gen_frequency * np.power(interval_to_divide, i/(number_of_divisions))) for i in range(0, int((np.log(max_frequency/gen_frequency) * number_of_divisions) / np.log(interval_to_divide)))]
    print(scl)
    return scl
    
